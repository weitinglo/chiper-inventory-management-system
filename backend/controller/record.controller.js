const recordService = require("../service/record.service");
const logService = require("../service/log.service");
const materialService = require("../service/material.service");

const getAllRecords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await recordService.getAllRecords();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getRecordsOnPage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      const res = await recordService.getRecordsOnPage(page);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getRecordsOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await recordService.getRecordsOnKeywords(page,keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await recordService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};


const getTotalDataCount = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await recordService.getTotalDataCount();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};


const addRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const record = req.body.record; //record : data會跑到這
      const res = await recordService.addRecord(record); //functiont傳值到下一層
      materialService.substractMaterialQuantity(record.category,record.name,record.quantity) //減掉庫存的量
      logService.addLog(req, "新增領料", " 名稱-" + record.name + " 領料人id-" + record.user + " 數量-" + record.quantity); //記錄到歷史紀錄裡
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};



const deleteRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.query.id;
      const category = req.query.category;
      const name = req.query.name;
      const quantity = req.query.quantity;

      materialService.substractMaterialQuantity(category,name,-quantity) //減掉庫存的量
      const res = await recordService.deleteRecord(id);
      logService.addLog(req, "刪除領料", " 類型-" + category + " 名稱-" + name + " 數量-" + quantity, id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const editRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const record = req.body.record;
      const res = await recordService.editRecord(record);
      materialService.substractMaterialQuantity(record.category,record.name,record.quantity-record.originalQty) //減掉庫存的量
      logService.addLog(req, "更新領料", "更新資料:類型-" + record.category + " 名稱-" + record.name + " 領料人id-" + record.user + " 數量-" + record.quantity);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};



module.exports = {
  getAllRecords,
  getRecordsOnPage,
  getRecordsOnKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  addRecord,
  deleteRecord,
  editRecord,
};
