const materialService = require("../service/material.service");
const logService = require("../service/log.service");

const getAllMaterials = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await materialService.getAllMaterials();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getMaterialOnPage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      const res = await materialService.getMaterialOnPage(page);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getMaterialOnPageOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await materialService.getMaterialOnPageOnKeywords(page,keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};


const addMaterial = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const material = req.body.material;
      const count = await materialService.checkExistingCode(material.code);
      if(count>0){
        resolve({code:400, msg:"已經存在此代碼，請嘗試不同代碼"});
      }else{
        const res = await materialService.addMaterial(material);
        logService.addLog(req, "新增物料", "名稱-" + material.name + " 數量-" + material.quantity);
        resolve(res);
      }
     
    } catch (error) {
      reject(error);
    }
  });
};

const deleteMaterial = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.query.id;
      const res = await materialService.deleteMaterial(id);
      logService.addLog(req,"刪除物料","刪除物料ID:"+id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const editMaterial = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const material = req.body.material;
      const res = await materialService.editMaterial(material);
      logService.addLog(req, "編輯物料", "原本資料: 名稱-" + material.name + " 數量-" + material.quantity + " 編號-" + material.code + " 類型-" + material.category);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};


const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
    const keywords = req.query.keywords;
    const res = await materialService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};
module.exports = {
  getAllMaterials,getMaterialOnPage,getMaterialOnPageOnKeywords,addMaterial,deleteMaterial,editMaterial,getTotalDataCountOnKeywords
};
