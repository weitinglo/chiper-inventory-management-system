const authService = require("../service/auth.service");
const logService = require("../service/log.service");
const userService = require("../service/user.service");


const login = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const username = req.body.username;
      const password = req.body.password;
      const res = await authService.login(username,password);
      if(res.code==200)
        logService.addLog(req,"登入",username+"登入成功");
      else
        logService.addLog(req,"登入",username+"登入失敗");

      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getLogginedUser = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      if(req.session){
        let userId = req.session.passport.user.id;
        let loggedinUser = await userService.getUserById(userId);
        loggedinUser.password="";
        resolve(loggedinUser);
      }else{
        reject("need to login ")
      }
    
     
    } catch (error) {
      reject(error);
    }
  });
};



module.exports = {
  login,
  getLogginedUser,
};
