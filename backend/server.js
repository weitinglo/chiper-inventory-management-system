const express = require("express");
const session = require('express-session')

const app = express();
const bodyParser = require("body-parser");
const port = 3077;

const cors = require('cors')

/**************** UI Config ******************/
var cookieParser = require('cookie-parser');
// cookieParser middleware
app.use(cookieParser());

// 套用 middleware
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(session({
  secret: 'chiper9101',
  rolling: true,
  resave: true, 
  saveUninitialized: false,
  cookie: { maxAge: 60000*30 } //30 mins
}))
const passport = require('./middleware/auth.js');

// 初始化 Passport
app.use(passport.initialize())
// 如果要使用 login session 時需設定 - session() 需設定在 passport.session() 之前，以確保 session 能正確地被處理
app.use(passport.session())

app.use(cors({
  origin:true,
  credentials: true
}))

/**************** USER Functions ******************/

const authRoute = require('./route/auth.route');
const userRoute = require('./route/user.route');
const materialRoute = require('./route/material.route');
const recordRoute = require('./route/record.route');
const logRoute = require('./route/log.route');


app.use('/api/user', userRoute);
app.use('/api/material', materialRoute);
app.use('/api/auth', authRoute);
app.use('/api/record', recordRoute);
app.use('/api/log', logRoute);



 

app.get("/", (req, res) => {
  res.sendFile(process.cwd() + "/my-app/dist/index.html");
});

app.listen(port, () => {
  console.log(`Server listening on the port::${port}`);
});
