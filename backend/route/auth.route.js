const router = require("express").Router();
let passport = require("../middleware/auth");

const { getLogginedUser } = require("../controller/auth.controller");

/*login a user*/
router.post("/login", passport.authenticate("local"), async (req, res) => {
  try {
    let loggedinID = new Set();
    req.sessionStore.all(function (err, sessions) {
      let isAlreadyLoggedin = false;
      for (let i in sessions) {
        if (sessions[i].passport) {
          let user = sessions[i].passport.user;
          if (!loggedinID.has(user.id)) {
            loggedinID.add(user.id);
          } else {
            isAlreadyLoggedin = true;
          }
        }
      }
      if(!isAlreadyLoggedin){
        res.redirect(200, "/");
      }else{
        res.status(403).send("已經登入過了");
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

/*Get logged*/
router.get("/loggedin_user" , async (req, res) => {
  try {
    const results = await getLogginedUser(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*logout*/
router.post("/logout", async (req, res) => {
  try {
    req.logout(function (err) {
      if (err) {
        return next(err);
      }
      res.status(200).send("ok");
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

module.exports = router;
