const router = require("express").Router();
let passport = require("../middleware/auth");

const {
  addRecord,
  getAllRecords,
  getRecordsOnKeywords,
  getTotalDataCountOnKeywords,
  deleteRecord,
  editRecord,
  getRecordsOnPage,
  getTotalDataCount
  
} = require("../controller/record.controller");

const authenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/Login');
};


/*Get all record*/
router.get("/", async (req, res) => {
  try {
    const results = await getAllRecords();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get records on page*/
router.get("/on_page",authenticated,async (req, res) => {
  try {
    const results = await getRecordsOnPage(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get records on keywords and page*/
router.get("/keywords",authenticated,async (req, res) => {
  try {
    const results = await getRecordsOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get total record count on keywords*/
router.get("/keywords_total_count",authenticated,async (req, res) => {
  try {
    const results = await getTotalDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});


/*Get total record count*/
router.get("/total_count", async (req, res) => {
  try {
    const results = await getTotalDataCount();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Add a record*/
router.put("/", async (req, res) => {
  try {
    const results = await addRecord(req); //給到controller
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Delete a record*/
router.delete("/", async (req, res) => {
  try {
    const results = await deleteRecord(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit a record*/
router.post("/", async (req, res) => {
  try {
    const results = await editRecord(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
