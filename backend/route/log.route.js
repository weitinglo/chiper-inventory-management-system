const router = require("express").Router();
const { config } = require("webpack");
const {
  getAllLogs,
  getLogsOnPageOnKeywords,
  getTotalDataCountOnKeywords,
} = require("../controller/log.controller");

const authenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/Login');
};

/*Get all the Logs*/
router.get("/",authenticated, async (req, res) => {
  try {
    const results = await getAllLogs();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get Logs on Page*/
router.get("/keywords",authenticated, async (req, res) => {
  try {
    const results = await getLogsOnPageOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});


/*Get all the total count*/
router.get("/page_num", async (req, res) => {
  try {
    const results = await getTotalDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;