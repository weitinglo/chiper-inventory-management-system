const router = require("express").Router();
const {
  getAllUsers,
  getUsersOnPage,
  getUsersOnPageKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  addUser,
  deleteUser,
  editUser,
  resetUserPassword,
  getUserProfile,
  updateProfile,
  updatePassword
} = require("../controller/user.controller");

/*Get all the users*/
router.get("/", async (req, res) => {
  try {
    //1
    const results = await getAllUsers();
    //6
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get users on page*/
router.get("/on_page", async (req, res) => {
  try { 
    const results = await getUsersOnPage(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get users on page*/
router.get("/keywords", async (req, res) => {
  try { 
    const results = await getUsersOnPageKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});


/*Get total data count of users on keywords*/
router.get("/keywords_total_count", async (req, res) => {
  try { 
    const results = await getTotalDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get total data count of users*/
router.get("/total_count", async (req, res) => {
  try { 
    const results = await getTotalDataCount();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Add a user*/
router.put("/", async (req, res) => {
  try {
    const results = await addUser(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Delete a user*/
router.delete("/", async (req, res) => {
  try {
    const results = await deleteUser(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit a user*/
router.post("/", async (req, res) => {
  try {
    const results = await editUser(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Rest User Password*/
router.post("/resetPassword", async (req, res) => {
  try {
    const results = await resetUserPassword(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});


/*Get profile of loggedin user*/
router.get("/profile", async (req, res) => {
  try { 
    const results = await getUserProfile(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit user profile*/
router.post("/profile", async (req, res) => {
  try {
    const results = await updateProfile(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit user password*/
router.post("/password", async (req, res) => {
  try {
    const results = await updatePassword(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});




module.exports = router;
