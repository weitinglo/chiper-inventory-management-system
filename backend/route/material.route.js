const router = require("express").Router();
const {
  getAllMaterials,
  getMaterialOnPage,
  getMaterialOnPageOnKeywords,
  getTotalDataCountOnKeywords,
  addMaterial,
  deleteMaterial,
  editMaterial,
  getTotalDataCount
} = require("../controller/material.controller");

const authenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/Login');
};


/*Get all the materials*/
router.get("/", async (req, res) => {
  try {
    const results = await getAllMaterials();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get materials on page*/
router.get("/on_page",authenticated, async (req, res) => {
  try {
    const results = await getMaterialOnPage(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get materials on page keywords*/
router.get("/keywords",authenticated, async (req, res) => {
  try {
    const results = await getMaterialOnPageOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get materials counts on keyworkds*/
router.get("/keywords_total_count", async (req, res) => {
  try {
    const results = await getTotalDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});


/*Add a user*/
router.put("/", async (req, res) => {
  try {
    const results = await addMaterial(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Delete a user*/
router.delete("/", async (req, res) => {
  try {
    const results = await deleteMaterial(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit a user*/
router.post("/", async (req, res) => {
  try {
    const results = await editMaterial(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});



module.exports = router;
