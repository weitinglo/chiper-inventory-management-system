const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");

const getAllMaterials = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT * FROM MATERIAL`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getMaterialOnPage = (page) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT * FROM MATERIAL ORDER BY ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery,[offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const checkExistingCode = (code) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) AS count FROM MATERIAL WHERE CODE = ?`;
    db.all(sqlQuery,[code], (err, rows) => {
      if (!err) {
        resolve(rows[0].count);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};


const getMaterialOnPageOnKeywords = (page,keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT * FROM MATERIAL WHERE (CATEGORY LIKE ? OR NAME LIKE ? OR CODE LIKE ?) ORDER BY ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery,['%' + keywords + '%','%' + keywords + '%','%' + keywords + '%',offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};




const addMaterial = (material) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `INSERT INTO MATERIAL(CATEGORY,NAME,CODE,STANDARD,QUANTITY,UNIT,FILES,NOTES) VALUES(?,?,?,?,?,?,?,?);`;
    db.all(sqlQuery, [material.category, material.name, material.code, material.standard, material.quantity, material.unit, material.files, material.notes], (err, rows) => {

        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
};

const deleteMaterial = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `DELETE FROM MATERIAL WHERE ID = ? ;`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const editMaterial = (material) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `UPDATE MATERIAL SET CATEGORY=?, NAME=?, CODE=?, STANDARD=?, QUANTITY=?, UNIT=?, FILES=?, NOTES=? WHERE ID = ?;`;
    db.all(sqlQuery, [material.category, material.name,material.code, material.standard, material.quantity, material.unit, material.files, material.notes, material.id], (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
};

const substractMaterialQuantity = (category,name,number) =>{
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `UPDATE MATERIAL SET QUANTITY=QUANTITY-? WHERE CATEGORY = ? AND NAME = ?;`;
    db.all(sqlQuery, [number,category,name], (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
}

const substractMaterialQuantityById = (id,number) =>{
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `UPDATE MATERIAL SET QUANTITY=QUANTITY-? WHERE ID = ?;`;
    db.all(sqlQuery, [number,id], (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
}

const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) as total FROM MATERIAL WHERE (CATEGORY LIKE ? OR NAME LIKE ? OR CODE LIKE ?)`;
    db.all(sqlQuery,['%' + keywords + '%','%' + keywords + '%','%' + keywords + '%'], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  getAllMaterials,
  getMaterialOnPage,
  getMaterialOnPageOnKeywords,
  checkExistingCode,
  addMaterial,
  deleteMaterial,
  editMaterial,
  substractMaterialQuantity,
  substractMaterialQuantityById,
  getTotalDataCountOnKeywords
};
