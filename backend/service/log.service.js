const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");

const getAllLogs = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT * FROM LOG
    `;

    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getLogsOnPageOnKeywords = (page,keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT * FROM LOG WHERE (ACTION LIKE ? OR USER LIKE ? OR CONTENT LIKE ? OR DATETIME LIKE ?) ORDER BY ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, ['%' + keywords + '%','%' + keywords + '%','%' + keywords + '%','%' + keywords + '%',offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const addLog = (req,action, content) => {
  let user = typeof(req.session.passport.user.name) != "undefined" ? req.session.passport.user.name:"";
  let userId =typeof(req.session.passport.user.id)  != "undefined" ? req.session.passport.user.id:"";
  var today = new Date();
  var date =
    today.getFullYear() +
    "-" +
    String(today.getMonth() + 1).padStart(2, "0") +
    "-" +
    String(today.getDate()).padStart(2, "0");
  var time =
    String(today.getHours()).padStart(2, "0") +
    ":" +
    String(today.getMinutes()).padStart(2, "0") +
    ":" +
    String(today.getSeconds()).padStart(2, "0");
  var dateTime = date + " " + time;

  let sqlQuery = `INSERT INTO LOG(ACTION,USER,CONTENT,DATETIME) VALUES(?,?,?,?);`;
  db.all(sqlQuery, [action, user+"-"+userId, content, dateTime], (err, rows) => {
  });
};

const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) as total FROM LOG WHERE (ACTION LIKE ? OR USER LIKE ? OR CONTENT LIKE ? OR DATETIME LIKE ?)
    `;
    db.all(sqlQuery,['%' + keywords + '%','%' + keywords + '%','%' + keywords + '%','%' + keywords + '%'], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  getAllLogs,
  getLogsOnPageOnKeywords,
  addLog,
  getTotalDataCountOnKeywords,
};
