const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");
const jwt = require("jsonwebtoken");

const jwtSecret = "chiper9101";
var bcrypt = require("bcrypt");

const login = (username, password) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `SELECT ID, PASSWORD,LEVEL FROM USER WHERE USERNAME = ?;`;
    db.all(sqlQuery, [username], (err, rows) => {
      if (!err) {
        if (rows.length > 0) {
          let dbPassword = rows[0].password;
 
          const verified = bcrypt.compareSync(password,dbPassword);
          let level = rows[0].level;
          if (!verified) {

            jsonRes.code = 401;
            jsonRes.msg = "密碼錯誤";
          } else {

            let user = {
              username: username,
            };
            var token = jwt.sign(user, jwtSecret, {
              expiresIn: 60 * 60 * 24,
            });
            let id = rows[0].id;
            jsonRes.token = token;
            jsonRes.username = username;
            jsonRes.level = level;
            jsonRes.id = id;

            
          }
        } else {
          jsonRes.msg = "沒有此使用者";
          jsonRes.code = 401;
        }
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  login,
};
