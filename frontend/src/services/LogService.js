const axios = require("axios");

export async function getAllLogs() {
  return await axios
    .get("/api/log")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getLogsOnPageOnKeywords(page, keywords) {
  return await axios
    .get("/api/log/keywords?page=" + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCountOnKeywords(keywords) {
  return await axios
    .get("/api/log/page_num?keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}
