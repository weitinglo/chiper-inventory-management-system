const axios = require("axios");

export async function getAllUsers() {
  return await axios
    .get("/api/user")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getUsersOnPage(page) {
  return await axios
    .get("/api/user/on_page?page=" + page)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getUsersOnPageKeywords(page, keywords) {
  return await axios
    .get("/api/user/keywords?page=" + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCountOnKeywords(keywords) {
  return await axios
    .get("/api/user/keywords_total_count?keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCount() {
  return await axios
    .get("/api/user/total_count")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function addUser(data) {
  //再進到下一層
  return await axios
    .put(`/api/user`, { user: data })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    }); //之後data這包的東西可以叫做user
}

export async function deleteUser(id) {
  return await axios
    .delete(`/api/user?id=` + id)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function resetUserPassword(id) {
  return await axios
    .post(`/api/user/resetPassword`, { id: id })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function editUser(user) {
  return await axios
    .post(`/api/user`, { user: user })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getUserProfile() {
  return await axios
    .get("/api/user/profile")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function updateProfile(profile) {
  return await axios
    .post(`/api/user/profile`, { profile: profile })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function updatePassword(password) {
  return await axios
    .post(`/api/user/password`, { password: password })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}
