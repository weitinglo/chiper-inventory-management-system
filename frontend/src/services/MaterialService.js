const axios = require("axios");

export async function getAllMaterials() {
  return await axios
    .get("/api/material")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getMaterialOnPage(page) {
  return await axios
    .get("/api/material/on_page?page=" + page)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getMaterialOnPageOnKeywords(page, keywords) {
  return await axios
    .get("/api/material/keywords?page=" + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCountOnKeywords(keywords) {
  return await axios
    .get("/api/material/keywords_total_count?keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCount() {
  return await axios
    .get("/api/material/total_count")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function addMaterial(data) {
  return await axios
    .put(`/api/material`, { material: data })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function deleteMaterial(id) {
  return await axios
    .delete(`/api/material?id=` + id)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function editMaterial(material) {
  return await axios
    .post(`/api/material`, { material: material })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getPageNum() {
  return await axios
    .get("/api/material/pageNum")
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}
